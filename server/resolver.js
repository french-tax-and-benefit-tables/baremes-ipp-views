/** Reads a table description from the configuration files and fetch the relevant data from the OpenFisca API */

const fetch = require("isomorphic-unfetch")
const isString = require("lodash.isstring")
const mapValues = require("lodash.mapvalues")
const isArray = require("lodash.isarray")
const Promise = require("bluebird")

async function resolveTable(tableDesc) {
  if (!isString(tableDesc)) {
    return await resolveCustomTable(tableDesc)
  }
  return await resolveParam(tableDesc)
}

async function resolveCustomTable(tableDesc) {
  const tableWithResolvedChildren = await Promise.props(
    mapValues(tableDesc, (value) => resolveTable(value)),
  )
  const subparams = mapValues(
    tableWithResolvedChildren,
    (child, description) => {
      return Object.assign({}, child, { description })
    },
  )
  return { subparams }
}

function makeSubsection(node, depth) {
  node.subparams = mapValues(node.subparams, (child) => {
    if (depth == 0 || !child.subparams) {
      return { table: child }
    }
    return makeSubsection(child, depth - 1)
  })
  return node
}

async function resolveSection(sectionDesc) {
  if (sectionDesc.table) {
    const table = await resolveTable(sectionDesc.table)
    return Object.assign({}, sectionDesc, { table })
  }
  if (sectionDesc.subsection) {
    const node = await resolveParam(sectionDesc.subsection)
    return Object.assign(
      {},
      sectionDesc,
      makeSubsection(node, sectionDesc.depth || 0),
    )
  }
  const resolvedChildren = isArray(sectionDesc.subparams)
    ? await Promise.all(sectionDesc.subparams.map(resolveSection))
    : await Promise.props(
        mapValues(sectionDesc.subparams, (child) => resolveSection(child)),
      )
  return Object.assign({}, sectionDesc, { subparams: resolvedChildren })
}

async function resolveParam(key) {
  const param = await fetchParam(key)
  if (param.values) {
    // Series

    let firstDate = "9999-01-01"
    let lastDate = "0001-01-01"
    for (const date of Object.keys(param.values)) {
      if (date < firstDate) {
        firstDate = date
      }
      if (date > lastDate) {
        lastDate = date
      }
    }
    param.first_year = parseInt(firstDate.split("-")[0])
    if (param.values[lastDate] === null) {
      param.last_year = parseInt(lastDate.split("-")[0])
      if (lastDate.endsWith("-01-01")) {
        param.last_year--
      }
    }

    return param
  }
  if (param.brackets) {
    // Scale

    // TODO: Detect first & last years.

    return param
  }
  if (param.subparams) {
    // Node
    const resolvedChildren = await Promise.props(
      mapValues(param.subparams, (childParam, childKey) =>
        resolveParam(`${param.id}.${childKey}`),
      ),
    )
    param.subparams = resolvedChildren

    let firstYear = 9999
    let lastYear = 1
    for (const subparam of Object.values(param.subparams)) {
      if (
        subparam.first_year !== undefined &&
        subparam.first_year < firstYear
      ) {
        firstYear = subparam.first_year
      }
      if (
        lastYear !== undefined &&
        (subparam.last_year === undefined || subparam.last_year > lastYear)
      ) {
        lastYear = subparam.last_year
      }
    }
    param.first_year = firstYear
    if (lastYear !== undefined) {
      param.last_year = lastYear
    }

    return param
  }
}

async function fetchParam(key, previousTries = 0) {
  try {
    const response = await fetch(`http://localhost:2000/parameter/${key}`)
    return await response.json()
  } catch (error) {
    // Try again, to deal with Mac race-conditions
    if (previousTries <= 5) {
      return fetchParam(key, (previousTries = previousTries + 1))
    } else {
      throw error
    }
  }
}

module.exports = {
  resolveTable,
  resolveSection,
  resolveParam,
}
