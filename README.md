# [Barèmes IPP](https://www.ipp.eu/baremes-ipp/)

Application web permettant de générer des tables de paramètres à partir d'[OpenFisca-France](https://github.com/openfisca/openfisca-france).

**CE PROJET A MIGRÉ VERS https://gitlab.com/ipp/partage-public-ipp/baremes-ipp/baremes-ipp-views.**

# Configuration des tables

Voir [documentation spécifique](./config-doc.md).

# Génération de l'app

L'application est générée sous forme de site HTML statique à partir de la configuration YAML.

Cette phase de génération utilise `Node.js` et `Next.js`

Une fois l'app générée, elle peut être servie directement par un serveur HTTP simple (sans `Node.js`).

## Prérequis

- `node`
- `yarn` (ou `npm`)
- Web-API d'OpenFisca servie sur `localhost:2000`

Le dépôt [baremes-ipp-yaml](https://gitlab.com/ipp/partage-public-ipp/baremes-ipp/baremes-ipp-yaml) fournit les paramètres de la législation nécessaires à la génération du site statique, ainsi qu'une version conteneurisée de l'API d'OpenFisca.

Voici une manière de faire tourner ce projet localement:

```bash
cd baremes-ipp-views
echo "TABLES_DIR=../baremes-ipp-yaml/tables/" >> .env

cd ..
git clone https://gitlab.com/ipp/partage-public-ipp/baremes-ipp/baremes-ipp-yaml.git
cd baremes-ipp-yaml
docker-compose up -d # Fait tourner OpenFisca Web API sur le port 2000
```

Pour tester OpenFisca Web API:

```bash
$ curl http://localhost:2000/
{"welcome":"This is the root of an OpenFisca Web API. To learn how to use it, check the general documentation (https://openfisca.org/doc/) and the OpenAPI specification of this instance (http://localhost:2000/spec)."}

$ curl http://localhost:2000/parameter/chomage
{"description":null,"id":"chomage","metadata":{"order":["allocations_assurance_chomage","allocations_chomage_solidarite","preretraites"]},"source":"https://gitlab.com/ipp/partage-public-ipp/baremes-ipp/baremes-ipp-yaml/blob/master/openfisca_baremes_ipp/parameters/chomage","subparams":{"allocations_assurance_chomage":{"description":"Allocations d'assurance chômage"},"allocations_chomage_solidarite":{"description":"Allocations chômage de solidarité"},"preretraites":{"description":"Préretraites"}}}
```

Pour stopper OpenFisca Web API:

```bash
# depuis baremes-ipp-yaml:
docker-compose down
```

## Build

```sh
yarn && yarn build && yarn export
```

génère un repertoire `out` qui contient le site statique à servir.

Pour visualiser le site généré, on peut le servir avec [sirv-cli](https://github.com/lukeed/sirv) :

```bash
npx sirv-cli --port 8080 out
# Ouvrir http://localhost:8080/ dans votre navigateur
```

## Configuration de l'app

Des variables de configuration peuvent être spécifiées dans un fichier `.env` situé à la racine du répertoire.

> Voir cet [exemple](https://gitlab.com/ipp/partage-public-ipp/baremes-ipp/baremes-ipp-views/blob/master/.env-prod), utilisé pour l'intrégration dans le site WordPress [ipp.eu](https://ipp.eu/)

Les valeurs par défault conviennent pour un site indépendant située à la racine du domaine.

- `BASENAME`: À spécifier si les barèmes sous servis dans un sous-répertoire du domaine (e.g. domaine.org/baremes)
- `WORDPRESS`: Si spécifié, génère un site optimisé pour l'intégration dans Wordpress
  - Les pages ne sont pas stylées, ne contiennent pas de header ni de JS, etc.
  - Les fichiers XLSX et CSV sont générés statiquement
- `TABLES_DIR`: Le dossier où sont configurés les fichiers de configuration des tables.

  Dans le cas où le mode `WORDPRESS` est activé, d'autres variables sont nécessaires:

- `CSV_PATH`: Sous-répertoire où sont exposées les tables générées statiquement.
- `BASENAME_EN_SECTIONS`: à spécifier si les pages de sections en anglais sont servies dans un sous-dossier différent des pages en français
- `BASENAME_EN_TABLES`: à spécifier si les pages de tables en anglais sont servies dans un sous-dossier différent des pages en français
  - Ce paramètre est légèrement différent du précédent dans la configuration actuellement en production, pour contourner une incompatibilité entre le module d'i18n (WPML) et la réécriture d'URL.
