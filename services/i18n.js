function getLongTitle(parameter, lang, defaultToId = true) {
  let title = undefined
  if (typeof parameter.title === "string") {
    title = parameter.title
  }
  if (
    !title &&
    parameter.title != null &&
    typeof parameter.title === "object"
  ) {
    title = parameter.title[lang] || parameter.title.fr
  }
  if (!title) {
    title =
      lang &&
      parameter.metadata &&
      (parameter.metadata[`label_${lang}`] ||
        parameter.metadata[`short_label_${lang}`])
  }
  if (!title) {
    title =
      parameter.description ||
      (parameter.metadata && parameter.metadata.short_label)
  }
  if (!title && defaultToId) {
    title = parameter.id
  }
  return title
}

function getShortTitle(parameter, lang, defaultToId = true) {
  let title = undefined
  if (typeof parameter.title === "string") {
    title = parameter.title
  }
  if (
    !title &&
    parameter.title != null &&
    typeof parameter.title === "object"
  ) {
    title = parameter.title[lang] || parameter.title.fr
  }
  if (!title) {
    title =
      lang &&
      parameter.metadata &&
      (parameter.metadata[`short_label_${lang}`] ||
        parameter.metadata[`label_${lang}`])
  }
  if (!title) {
    title =
      (parameter.metadata && parameter.metadata.short_label) ||
      parameter.description
  }
  if (!title && defaultToId) {
    title = parameter.id
  }
  return title
}

module.exports = { getLongTitle, getShortTitle }
