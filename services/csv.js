const d3 = require("d3-dsv")
const isServerSide = typeof window === "undefined"
const Excel = isServerSide
  ? require("exceljs")
  : require("exceljs/dist/es5/exceljs.browser")
const mapValues = require("lodash.mapvalues")
const mapKeys = require("lodash.mapkeys")
const keys = require("lodash.keys")
const isPlainObject = require("lodash.isplainobject")
const last = require("lodash.last")

function cleanCSVValue(value) {
  if (Array.isArray(value)) {
    return value.map(cleanCSVValue).join(" | ")
  }
  if (isPlainObject(value)) {
    if (value.value !== undefined) {
      return value.value
    }
    // reference or notes
    return [value.title, value.href]
      .filter((item) => item !== undefined)
      .join("; ")
  }
  return value
}

function cleanCSVValues(datum) {
  return mapValues(datum, cleanCSVValue)
}

function cleanKeys(datum, path) {
  return mapKeys(datum, (value, key) => {
    return key.replace(new RegExp("^" + path + "\\."), "")
  })
}

function cleanDatum(datum, path) {
  return cleanKeys(cleanCSVValues(datum), path)
}

function toCSV(tableData, parameterId) {
  const data = tableData.map((datum) => cleanDatum(datum, parameterId))
  return d3.csvFormat(data)
}

function cleanXLSXValue(value) {
  if (Array.isArray(value)) {
    return cleanXLSXValue(value[0])
  }
  if (isPlainObject(value)) {
    if (value.value !== undefined) {
      return value.value
    }
    // reference or notes
    if (value.href) {
      return {
        hyperlink: value.href,
        text: value.title || value.href,
      }
    }
    return value.title
  }
  return value
}

function cleanXLSXValues(datum) {
  return mapValues(datum, cleanXLSXValue)
}

function getCell(sheet, row, col) {
  return sheet.getRow(row + 1).getCell(col + 1)
}

function getFreeCellCol(sheet, row, minCol) {
  const cell = getCell(sheet, row, minCol)
  if (!isMerged(cell)) {
    return minCol
  }
  return getFreeCellCol(sheet, row, minCol + 1)
}

function isMerged(cell) {
  return cell.master != cell
}

function toXLSX(table, tableName) {
  const workbook = new Excel.Workbook()
  const sheet = workbook.addWorksheet(tableName)
  sheet.columns = table.columns.map((column) => ({
    key: column.id,
    width: (column.width || 1) * 20,
  }))

  // Fill the header
  table.headers.forEach((headerRow, row) => {
    let col = 0
    for (const headerCell of headerRow) {
      col = getFreeCellCol(sheet, row, col)
      const cell = getCell(sheet, row, col)
      cell.value =
        typeof headerCell.Header === "string"
          ? headerCell.Header
          : headerCell.Header.longTitle
      cell.alignment = {
        wrapText: true,
        vertical: "middle",
        horizontal: "center",
      }
      sheet.mergeCells(
        row + 1,
        col + 1,
        row + (headerCell.rowSpan || 1),
        col + headerCell.colSpan,
      )
      col += headerCell.colSpan
    }
  })

  // Fill the data
  table.data.forEach((datum) => sheet.addRow(cleanXLSXValues(datum)))

  return workbook
}

module.exports = { toCSV, toXLSX, cleanDatum }
