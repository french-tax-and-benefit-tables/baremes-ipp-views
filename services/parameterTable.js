const extractData = require("./tableifier").extractData
const { buildColumns } = require("./columnBuilder")
const { buildHeaders } = require("./headerBuilder")

function parameterTable(parameter, lang) {
  const data = extractData(parameter)
  const {documentation, nestedColumns} = buildColumns(parameter, lang)
  const { headers, columns } = buildHeaders(nestedColumns)
  return {
    columns,
    headers,
    data,
    documentation,
  }
}

module.exports = {
  parameterTable,
}
