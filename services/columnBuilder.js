// Build parameterTable columns

const map = require("lodash.map")
const filter = require("lodash.filter")
const range = require("lodash.range")
const size = require("lodash.size")
const sortBy = require("lodash.sortby")
const flow = require("lodash.flow")

const msg = require("../messages")
const { getLongTitle, getShortTitle } = require("./i18n")

function buildColumns(parameter, lang) {
  const indexes = []
  let match
  const regexp = /\((\d{1,2})\)/g
  while ((match = regexp.exec(parameter.documentation || "")) !== null) {
    indexes.push(parseInt(match[1]))
  }
  const lastDocumentationIndex = indexes.reduce(
    (maxIndex, index) => Math.max(index, maxIndex),
    0,
  )
  const documentationContext = {
    fragments: [],
    merge: parameter.metadata.documentation_start,
    nextIndex: lastDocumentationIndex + 1,
  }

  const dateColumn = {
    Header: msg.date[lang],
    accessor: (item) => item.date,
    id: "date",
  }

  const dataColumn = buildColumn(parameter, lang, documentationContext)

  const mergedMetadata = mergeMetadata(parameter)
  for (const [name, value] of Object.entries(mergedMetadata)) {
    if (Object.keys(value).length === 0) {
      delete mergedMetadata[name]
    }
  }

  return {
    documentation: parameter.metadata.documentation_start
      ? documentationContext.fragments
          .map((documentation) => documentation.replace(/\n+$/, ""))
          .join("\n")
      : parameter.documentation,
    nestedColumns: [
      dateColumn,
      dataColumn,
      ...buildMetaDataColumns(mergedMetadata, lang),
    ],
  }
}

function buildColumn(parameter, lang, documentationContext) {
  let longTitle = getLongTitle(parameter, lang)
  let shortTitle = getShortTitle(parameter, lang)
  if (documentationContext.merge && parameter.documentation) {
    if (documentationContext.isSubparam) {
      documentationContext.fragments.push(
        `(${documentationContext.nextIndex}) ${parameter.documentation}`,
      )
      longTitle = `${longTitle} (${documentationContext.nextIndex})`
      shortTitle = `${shortTitle} (${documentationContext.nextIndex})`
      documentationContext.nextIndex++
    } else {
      documentationContext.fragments.push(parameter.documentation)
    }
  }
  let header = shortTitle === longTitle ? longTitle : { longTitle, shortTitle }

  if (parameter.values) {
    // Series
    // The structure of the repo is not the typical OF structure, so we monkey patch source:
    const source = parameter.source.replace("openfisca_baremes_ipp/", "")
    return {
      Header: header,
      source,
      accessor: (item) => item[parameter.id],
      id: parameter.id,
    }
  }
  if (parameter.brackets) {
    // Scale
    const maxNbTranche = Math.max(
      ...map(parameter.brackets, (bracket) => size(bracket)),
    )
    return {
      Header: header,
      columns: map(range(maxNbTranche), (index) => {
        return {
          Header: `Tranche ${index}`,
          columns: [
            {
              Header: "Seuil",
              accessor: (item) => item[`${parameter.id}.${index}.threshold`],
              id: `${parameter.id}.${index}.threshold`,
            },
            {
              Header: "Valeur",
              accessor: (item) => item[`${parameter.id}.${index}.value`],
              id: `${parameter.id}.${index}.value`,
            },
          ],
        }
      }),
    }
  }
  if (parameter.subparams) {
    // Node
    documentationContext.isSubparam = true
    return {
      Header: header,
      columns: flow([
        (x) =>
          map(x, (subParam, name) => Object.assign({}, subParam, { name })),
        (x) =>
          sortBy(
            x,
            (subParam) =>
              parameter.metadata &&
              parameter.metadata.order &&
              parameter.metadata.order.indexOf(subParam.name),
          ),
        (x) =>
          map(x, (param) => buildColumn(param, lang, documentationContext)),
      ])(parameter.subparams),
    }
  }
}

function buildMetaDataColumns(metadata, lang) {
  const columnInfosByMetadataName = {
    reference: { title: msg.references[lang], width: 1.8 },
    official_journal_date: { title: msg.parutionJO[lang], width: 1 },
    notes: { title: "Notes", width: 3.5 },
  }
  return flow([
    (x) => filter(x, (fieldName) => metadata[fieldName]),
    (x) =>
      map(x, (fieldName) => ({
        Header: columnInfosByMetadataName[fieldName].title,
        accessor: () => metadata[fieldName],
        id: fieldName,
        width: columnInfosByMetadataName[fieldName].width,
      })),
  ])(Object.keys(columnInfosByMetadataName))
}

function mergeMetadata(
  parameter,
  mergedMetadata = { reference: {}, official_journal_date: {}, notes: {} },
) {
  for (const [name, mergedValuesByDate] of Object.entries(mergedMetadata)) {
    const valueByDate = parameter.metadata[name]
    if (valueByDate != null) {
      for (const [date, value] of Object.entries(valueByDate)) {
        if (value != null) {
          const mergedValues = (mergedValuesByDate[date] =
            mergedValuesByDate[date] || [])
          const values = Array.isArray(value) ? value : [value]
          for (const value of values) {
            if (
              mergedValues.find((mergedValue) => {
                if (mergedValue === value) {
                  return true
                }
                if (typeof mergedValue !== typeof value) {
                  return false
                }
                if (
                  typeof mergedValue === "object" &&
                  Object.keys(mergedValue).length === Object.keys(value).length
                ) {
                  for (const [itemKey, itemValue] of Object.entries(
                    mergedValue,
                  )) {
                    if (itemValue !== value[itemKey]) {
                      return false
                    }
                  }
                  return true
                }
                return false
              }) === undefined
            ) {
              mergedValues.push(value)
            }
          }
        }
      }
    }
  }
  if (parameter.subparams) {
    for (const subparam of Object.values(parameter.subparams)) {
      mergedMetadata = mergeMetadata(subparam, mergedMetadata)
    }
  }
  return mergedMetadata
}

module.exports = {
  buildColumns,
}
