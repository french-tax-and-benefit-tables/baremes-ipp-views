import { withRouter } from "next/router"
import map from "lodash.map"
import isArray from "lodash.isarray"
import includes from "lodash.includes"
import flow from "lodash.flow"
import sortBy from "lodash.sortby"
import { Component } from "react"

import { basename, basenameEnTables, isWP } from "../config"
import msg from "../messages"
import Layout from "../components/Layout"
import LangToggle from "../components/LangToggle"
import { getLongTitle, getShortTitle } from "../services/i18n"

const dateRangeRegExp = /\s*\(\s*(\d+(\s*-\s*(\d+)?)?|depuis\s+\d+)\s*\)\s*$/

class Section extends Component {
  // Define computed properties

  get section() {
    return this.props.router.query.section
  }

  get path() {
    const router = this.props.router
    return router.asPath.endsWith("/") ? router.asPath : router.asPath + "/"
  }

  get subParams() {
    return flow([
      (x) => map(x, (subParam, name) => Object.assign({}, subParam, { name })),
      (x) =>
        sortBy(x, (subParam) =>
          this.section?.metadata?.order?.indexOf(subParam.name),
        ),
    ])(this.section.subparams)
  }

  get lang() {
    return this.props.router.query.lang || "fr"
  }

  // Render

  render() {
    if (isWP) {
      return this.renderSectionContent()
    }
    return (
      <Layout>
        <LangToggle
          lang={this.lang}
          target={basename + this.props.router.query.translationPage}
        />
        <h1 className="box">
          <span>{getLongTitle(this.section, this.lang)}</span>
        </h1>
        <div className="entry-content text">{this.renderSectionContent()}</div>
      </Layout>
    )
  }

  renderSectionContent() {
    const i18nBasename =
      this.lang == "fr" ? basename : basenameEnTables || basename // In WP mode, the basename for English tables pages is specific
    return (
      <div>
        <h4>{msg.sommaire[this.lang]}</h4>
        <ol>
          {map(this.subParams, (subParam) =>
            this.renderItem(
              subParam,
              subParam.name,
              `${i18nBasename}${this.path}`,
              0,
            ),
          )}
        </ol>
      </div>
    )
  }

  renderItem(item, key, path, depth) {
    if (item.subparams && item.flat) {
      return this.renderSubParams(item, key, path, depth)
    }
    if (item.subparams) {
      const dateRange =
        item.last_year === undefined
          ? ` (depuis ${item.first_year})`
          : item.last_year === item.first_year
          ? ` (${item.first_year})`
          : ` (${item.first_year}-${item.last_year})`
      const longTitle = getLongTitle(item, this.lang).replace(
        dateRangeRegExp,
        "",
      )
      const shortTitle = getShortTitle(item, this.lang).replace(
        dateRangeRegExp,
        "",
      )
      return (
        <li key={key}>
          {depth === 0 || shortTitle === longTitle ? (
            longTitle
          ) : (
            <abbr title={longTitle}>{shortTitle}</abbr>
          )}
          {dateRange}
          <ol>{this.renderSubParams(item, key, path, depth + 1)}</ol>
        </li>
      )
    }
    if (item.table) {
      const firstYear = item.first_year || item.table.first_year
      const lastYear = item.first_year ? item.last_year : item.table.last_year
      const dateRange =
        firstYear === undefined
          ? ""
          : lastYear === undefined
          ? ` (depuis ${firstYear})`
          : lastYear === firstYear
          ? ` (${firstYear})`
          : ` (${firstYear}-${lastYear})`
      const longTitle = (
        getLongTitle(item, this.lang) || getLongTitle(item.table, this.lang)
      ).replace(dateRangeRegExp, "")
      const shortTitle = (
        getShortTitle(item, this.lang) || getShortTitle(item.table, this.lang)
      ).replace(dateRangeRegExp, "")
      return (
        <li key={key}>
          <a href={`${path}${key}`}>
            {depth === 0 || shortTitle === longTitle ? (
              longTitle
            ) : (
              <abbr title={longTitle}>{shortTitle}</abbr>
            )}
            {dateRange}
          </a>
        </li>
      )
    }
  }

  renderSubParams(item, key, path, depth) {
    const shouldSort = !isArray(item.subparams) // A specific order has been explicitly defined in the conf
    const subParams = flow([
      (x) => map(x, (subParam, name) => Object.assign({}, subParam, { name })),
      (x) =>
        shouldSort
          ? sortBy(x, (subParam) =>
              item?.metadata?.order?.indexOf(subParam.name),
            )
          : x,
    ])(item.subparams)

    return map(subParams, (subParam) => {
      if (item.exclude && includes(item.exclude, subParam.name)) {
        return
      }
      return this.renderItem(subParam, subParam.name, `${path}${key}/`, depth)
    })
  }
}

export default withRouter(Section)
